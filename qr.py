# This file contains methods to compute the distributed QR

import numpy as np
import anytree as tree
import scipy.linalg
import csv

import utils

def select_columns_lapack(A, k):
    q, r, p = scipy.linalg.qr(A, pivoting=True)
    residue = np.linalg.norm(q[:, k:].T @ A)
    selected_cols = p[:k]
    return selected_cols, residue


def truncated_qr_lapack(A, k):
    q, r, p = scipy.linalg.qr(A, pivoting=True)
    #permutation = np.eye(A.shape[0], A.shape[1])[:, p]
    #r = r @ permutation.T
    residue = np.linalg.norm(r[k:, k:])
    error_node = tree.Node("a", requested=0, error=residue, factor=1, increase_k=0, rank=k)

    return q[:, :k], q[:,:k].T @ A, error_node, p[:k]

def split_qr_rank(A, k, nb_cores, opt, depth=0):

    # Divide matrix
    split_2d = np.array(np.fromstring(nb_cores[0], dtype=int, sep="x"))

    if len(split_2d) == 1:
        row_reduction = np.int(np.sqrt(split_2d[0]))
        row_reduction = 2 ** np.int(np.log2(row_reduction))
        col_reduction = split_2d[0] // row_reduction
        split_2d = np.array([row_reduction, col_reduction])
        print("split_2d", split_2d)

    if len(split_2d) == 2 and split_2d[0] != 1 and split_2d[1] != 1:
        nb_cores = []
        for i in range(np.int(np.log2(split_2d[1]))):
            nb_cores.append("1x2")
        for i in range(np.int(np.log2(split_2d[0]))):
            nb_cores.append("2x1")
        print("nb_cores", nb_cores);

    split_2d = np.fromstring(nb_cores[0], dtype=int, sep="x")

    nb_cores_cur = 0
    if len(split_2d) == 2:
        posx, posy, start_i, start_j = utils.split_indexes_2D(A.shape[0], A.shape[1], split_2d[0], split_2d[1])
        nb_cores_cur = split_2d[0] * split_2d[1]
    else:
        print("split_2d", split_2d)
        raise ValueError("Specified splitting {} is incorrect. Format is 4 or 4x4".format(nb_cores[0]))

    a = []
    selected_columns = set()
    selected_cols_local_offset = []

    # huge error to make use of increase_k directly
    error = np.linalg.norm(A) + 1

    current_node = tree.Node("a", requested=0, increase_k=0, factor=1)

    # Split A
    for i in range(nb_cores_cur):
        a.append(A[start_i[posx[i]]:start_i[posx[i] + 1], start_j[posy[i]]:start_j[posy[i] + 1]])

    for i in range(nb_cores_cur):
        # Compute truncated pivoted QR to get submatrice columns
        if len(nb_cores) == 1:
            selected_cols_local, residue = select_columns_lapack(a[i], k)
            tree.Node("a", requested_k=k, rank=k, parent=current_node, requested=error, error=residue, posy=posy[i])
        else:
            selected_cols_local, residue, child_node = split_qr_rank(a[i], k, nb_cores[1:], opt, depth + 1)
            child_node.posy = posy[i]
            child_node.parent = current_node

        if len(selected_cols_local) == 0:
            selected_cols_local = [0]

        # Add offset to get absolute column number
        if opt.selected_cols:
            print("depth", depth, "submatrix", i, [x + start_j[posy[i]] for x in selected_cols_local])
        selected_cols_local_offset.append({x + start_j[posy[i]] for x in selected_cols_local if (x + start_j[posy[i]] not in selected_columns)})

        # Store to global selected column list
        selected_columns = selected_columns | selected_cols_local_offset[i]

    a_mini = A[:, list(selected_columns)]

    if depth > 0:
        step2_selected_cols, residue = select_columns_lapack(a_mini, k)

        current_node.rank = k
        current_node.error = residue

        return np.array(list(selected_columns))[step2_selected_cols], residue, current_node

    Q, R, residue, step2_selected_cols = truncated_qr_lapack(a_mini, k)
    current_node.error = residue.error
    current_node.rank = residue.rank

    print("final columns", np.array(list(selected_columns))[step2_selected_cols])

    return Q, Q.T @ A, current_node, np.array(list(selected_columns))[step2_selected_cols]

