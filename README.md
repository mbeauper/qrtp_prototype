This code tests the accuracy of the QRTP algorithm. For any provided matrix $A$ it computes the low rank approximation $A_k$ using QRTP and plots the singular values of the approximation.

The purpose is to reproduce the results of [1].

# Requirement

* python > 3.5 (you must have the `@` operator to do matrix multiplication)
* virtualenv (optional)


# Download and create the environment

Clone the repository

```bash
git clone https://gitlab.inria.fr/mbeauper/qrtp_prototype.git
```

## With virtualenv

Initiate environment

```bash
$ python3 -m venv qrtp-env
$ source qrtp-env/bin/activate
(qrtp-env) $ python -m pip install -r requirements.txt
```

## Without virtualenv

Install the following packages:
* numpy 1.18.5
* matplotlib 3.1.3
* pandas 1.0.1
* anytree 2.8.0
* scipy 1.4.1

```bash
pip3 install numpy matplotlib pandas anytree scipy
```

# How to reproduce the results from [1]

## Figure with a single reduction strategy

```bash
python3 main.py matrices/heat.txt -c 64 -k 50 -f
```

![Heat 50](images/heat_50.png)

```bash
python3 main.py matrices/gravity.txt -c 64 -k 50 -f
```

![Gravity 50](images/gravity_50.png)

## Figure with multiple reduction strategies

```bash
python3 main.py matrices/heat.txt matrices/gravity.txt -k 50 -c 1x2,1x2,1x2,2x1,2x1,2x1 2x1,2x1,2x1,1x2,1x2,1x2 1x8,8x1 8x1,1x8
python3 vizualisation.py
```

![Gravity](images/gravity.png)
![Heat](images/heat.png)

[1] Matthias Beaupère, Laura Grigori. Communication avoiding low rank approximation based on QR with tournament pivoting. 2020. ⟨[hal-02947991](https://hal.inria.fr/hal-02947991)⟩
