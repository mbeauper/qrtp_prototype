import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

data = pd.read_csv("data/singular_values.out", sep="\t")

data_by_matrix = data.groupby("matrix_name")
for matrix_name, data_matrix in data_by_matrix:
    plt.figure(figsize=(8,5))

    data_grouped = data_matrix.groupby("nb_cores")
    print_qrcp = False
    for name, group in data_grouped:
        data = group.loc[group['index_sv'] <= 50]
        if not print_qrcp:
            plt.plot(data["index_sv"], data["sv_qrcp"] / data["sv_svd"], label="QRCP")
            print_qrcp = True
        plt.plot(data["index_sv"], data["ratio"], label=name)

    plt.grid()
    plt.legend(["QRCP", "row-first of degree 2", "row-first of degree 8", "column-first of degree 2", "column-first of degree 8"]);
    plt.title(matrix_name)
    plt.xlabel("Singular value index")
    plt.ylabel(r"Error ratio $\sigma_i(A_k) / \sigma_i(A)$")
    plt.savefig("pdf/" + matrix_name + ".pdf")
    plt.show()
